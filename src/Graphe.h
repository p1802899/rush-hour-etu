#ifndef GRAPHE_H
#define GRAPHE_H

#include "Plateau.h"
#include <vector>
#include <queue>
#include <iostream>

class Graphe
{
    private:
        Plateau *m_depart;

        std::queue<Plateau*> m_file;

        std::vector<Plateau*> m_graph;

        Plateau *m_situationGagnante;

    public:
        Graphe(Plateau* depart);

        void parcoursLargeur();
        void afficheChemin(std::vector<Plateau*> chemin);
        Plateau* contientPlateau(Plateau* otherPlateau);
    ~Graphe();
    Plateau* getSommet();


    //Donne le chemin le plus cours depuis un point vers le sommet ( le debut du graphe )
    std::vector<Plateau*> shortestRoad(Plateau* p);
};

#endif