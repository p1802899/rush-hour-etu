#include "Vehicule.h"

Vehicule::Vehicule()
{
    m_ligne = 0;
    m_colonne = 0;
    m_longueur = 2;
    m_sens = 1;
}

Vehicule::Vehicule(int ligne, int colonne, int longueur, int sens)
{
    m_ligne = ligne;
    m_colonne = colonne;
    m_longueur = longueur;
    m_sens = sens;
}

Vehicule::Vehicule(Vehicule* v){
    m_ligne = v->getLigne();
    m_colonne = v->getColonne();
    m_longueur = v->getLongeur();
    m_sens = v->getSens();
}

void Vehicule::print_info()
{
    std::cout << "(" << m_ligne << ", " << m_colonne << ", " << m_longueur << ", " << m_sens << ")" << std::endl;
}

int Vehicule::getLongeur() const{
    return m_longueur;
}


int Vehicule::getSens() const{
    return m_sens;
}

int Vehicule::getLigne() const{
    return m_ligne;
}

int Vehicule::getColonne() const{
    return m_colonne;
}


void Vehicule::setPosition(int ligne, int colonne){
    m_ligne = ligne;
    m_colonne = colonne;
}



//Return true si les deu véhicules sont egau
bool Vehicule::vehiculeEgal(Vehicule* other){
    
    return (m_ligne == other->getLigne() && m_colonne == other->getColonne() && m_sens == other->getSens() && m_longueur == other->getLongeur()); 
}

