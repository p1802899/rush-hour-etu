#include "Plateau.h"
#include <string>
#include <cmath>

const float inf= std::numeric_limits<float>::infinity();

Plateau::Plateau()
{
    m_visite = false;
    m_nbCoups = inf;
    m_vehiculeSortant = nullptr;
}

//Constructeur pour créer un plateau via un tableau de vehicules
//Dans ce constructeur, le véhicule a sortir est déja dans le tableau de vehicules(m_véhicules)
Plateau::Plateau(int sortieC, int sortieL, Vehicule* vehiculeSortant, std::vector<Vehicule*> vehicules) : 
m_sortieC(sortieC),m_sortieL(sortieL), m_vehiculeSortant(vehiculeSortant), m_vehicules(vehicules)
{   
    m_visite = false;
    m_nbCoups = inf;

//Section qui génère un tableau 2D qui répresente la présences des véhicules dans le tableau
//Les cases à 1 sont des véhicule --
//Cases à 2 |
    for (int i = 0; i < 6; i++)
    {

        plateau_2d.push_back(std::vector<int>());

        for (int j = 0; j < 6; j++)
        {
           plateau_2d[i].push_back(0);
        }
    }


    m_vehiculeSortant = new Vehicule(vehiculeSortant);

    for (unsigned int i = 0; i < m_vehicules.size(); i++)
    {
        Vehicule* v = m_vehicules[i];

        int longeur = v->getLongeur();

        if(v->getSens() == 1){

            for (int i = v->getColonne(); i < longeur + v->getColonne() ; ++i)
            {
                plateau_2d[v->getLigne()][i] = 1;
            }
        }else{

            for (int i = v->getLigne(); i < longeur + v->getLigne() ; ++i)
            {
                plateau_2d[i][v->getColonne()] = 2;
            }

        }
    }

}


Plateau::Plateau(const std::string &filename)
{
    m_visite = false;
    m_nbCoups = 0;

    std::ifstream map(filename);

    
    if (map) {

        //position de la sortie
        map >> m_sortieL >> m_sortieC;
        int ligne, colonne, longueur, orientation;

        //le véhicule à déplacer jusqu'à la sortie
        map >> ligne >> colonne >> longueur >> orientation;
        m_vehiculeSortant = new Vehicule(ligne, colonne, longueur, orientation);
        
        m_vehicules.push_back(m_vehiculeSortant);

        //on lis les infos sur les autres véhicules jusqu'à atteindre la fin du fichier
        while (map >> ligne >> colonne >> longueur >> orientation) {
            Vehicule *v = new Vehicule(ligne, colonne, longueur, orientation);
            m_vehicules.push_back(v);
        }
    }

//Section qui génère un tableau 2D qui répresente la présences des véhicules dans le tableau
//Les cases à 1 sont des véhicule --
//Cases à 2 |

    
    for (int i = 0; i < 6; i++)
    {

        plateau_2d.push_back(std::vector<int>());

        for (int j = 0; j < 6; j++)
        {
           plateau_2d[i].push_back(0);
        }
    }


    for (unsigned int i = 0; i < m_vehicules.size(); i++)
    {
        Vehicule* v = m_vehicules[i];

        int longeur = v->getLongeur();

        if(v->getSens() == 1){

            for (int i = v->getColonne(); i < longeur + v->getColonne() ; ++i)
            {
                plateau_2d[v->getLigne()][i] = 1;
            }
        }else{

            for (int i = v->getLigne(); i < longeur + v->getLigne() ; ++i)
            {
                plateau_2d[i][v->getColonne()] = 2;
            }

        }
    }


}


Vehicule * Plateau::getVehiculeSortant(){
    return m_vehiculeSortant;
}


//teste si le véhicule sortant est au niveau de la sortie
bool Plateau::gagnant() {
    int finVehiculeC = (m_vehiculeSortant->getColonne() + m_vehiculeSortant->getLongeur()) - 1;
    
    if (finVehiculeC == m_sortieC) {
        return true;
    }
    return false;
}

void Plateau::affiche_info()
{
    std::cout << "position de la sortie : " << m_sortieL << ", " << m_sortieC << std::endl;
    std::cout << "véhicule sortant: ";
    m_vehiculeSortant->print_info();

    for (unsigned int i = 0; i < m_vehicules.size(); i++) {
        std::cout << "vehicule " << i << ": "; 
        m_vehicules[i]->print_info();
    }
}

void Plateau::setVisite(bool visite) {
    m_visite = visite;
}

bool Plateau::estVisite() const {
    return m_visite;
}

int Plateau::getNbCoups() const {
    return m_nbCoups;
}

std::vector<Vehicule *> Plateau::getVehicules(){
    return m_vehicules;
}


void Plateau::incrNbCoups(int i) {
    m_nbCoups = i;
}

void Plateau::affiche_plateau(){

    //Ligne d'étoile du haut du plateau 
    for(unsigned int i = 0; i<8; ++i){
        std::cout << "*";
    }
    std::cout << std::endl;
    for (unsigned int i = 0; i < 6; i++)
    {

        if(m_sortieL == (int) i && m_sortieC == 0){
            std::cout << " ";
        }else{
            std::cout << "*";
        }

        for (unsigned int j = 0; j < 6; j++)
        {
            if(plateau_2d[i][j] == 1){
                std::cout <<'-';
            }else if(plateau_2d[i][j] == 2){
                std::cout <<'|';
            }else{
                std::cout <<' ';
            }
        }


        if(m_sortieL == (int) i && m_sortieC == 5){
            std::cout << " -> Sortie";
        }else{
            std::cout << "*";
        }

        std::cout << std::endl;
    }
    
    //Ligne d'étoile du bas de plateau
    for(unsigned int i = 0; i<8; ++i){
        std::cout << "*";
    }

    std::cout << std::endl;

    // ***************
    // * |    |------*
    // * |    | |----*
    // * ----   |  | *
    // * ----      | *
    // * |     || | *
    // * |---- || | *
    // ***************
}


//Regarde si'l est possible de créer une nouvelle situation
//Parcourt la liste de tous les véhicules présents et regarde si un déplacement est possible
//Si oui, créer un plateau(situation) voisin et le met dans ca liste de plateau voisin
void Plateau::deplacement_vehicule(){
    bool nouveauVehiculeSortant;
    
    for(unsigned int i = 0; i < m_vehicules.size(); i++){

        nouveauVehiculeSortant = false; //Cette variable permet de déterminé si le véhicule manipulé est le véhicule sortant ou non
        Vehicule* v = m_vehicules[i];

        
        
        int longeur = v->getLongeur();
        //1 = le vehicule est horizontal
        if(v->getSens() == 1){               

            //Déplacement d'un véhicule horizontal vers la droite---------------------------------------------------------------
            int fin_vehicule = v->getColonne() + longeur -1;  //Le moins 1 permet de s'adapter au indices partant de 0
            int debut_vehicule = v->getColonne();


            int case_droite = fin_vehicule + 1;
            int case_gauche = debut_vehicule - 1;
            
            //regarde si le vehicule ne ce trouve pas déja au bord du plateau et que la case de droite est vide pour permettre un déplacement
            if(fin_vehicule < plateau_2d[v->getLigne()].size()-1 && plateau_2d[v->getLigne()][case_droite] < 1 ){

                while(case_droite < plateau_2d[v->getLigne()].size()-1 && plateau_2d[v->getLigne()][case_droite] < 1 ){
                    case_droite = case_droite + 1;
                }

                //Dans ce cas nous pouvons donc créer une nouvelle situation ou la voiture en question s'est déplacé 

                //On change la position du vehicule
                //on s'assure que les nouvelles valeurs représentes biens la partie a droite des véhicules horitontau

                // Pour eviter que toutes les situation partages les même pointeurs de véhicules

                
                Vehicule* v2 = new Vehicule(v);

                if(plateau_2d[v->getLigne()][case_droite] > 0){
                    v2->setPosition(v->getLigne(), (case_droite - longeur));   //+1 car on regardais une case avant la voiture avec case_droite

                }else{
                    v2->setPosition(v->getLigne(), (case_droite - longeur + 1));
                }

                //Pour avoir des pointeurs differents entre les éléments des situation, il faut copier tous les éléments
                //Recopie du tableau de véhicule
                std::vector<Vehicule*> nouveau_vehicules;

                for(int j = 0; j < m_vehicules.size(); ++j){
                    nouveau_vehicules.push_back(new Vehicule(m_vehicules[j]));
                }
 
                // Remlacer le véhicule par un autre véhicule avec les nouvelles coordonnées
                nouveau_vehicules[i] = v2;

                Plateau* p;

                if(v->vehiculeEgal(m_vehiculeSortant)){
                    p = new Plateau(m_sortieC, m_sortieL, v2, nouveau_vehicules);
                }else{
                    p = new Plateau(m_sortieC, m_sortieL, m_vehiculeSortant, nouveau_vehicules);
                }
                
                m_voisins.push_back(p);

                //p->affiche_plateau();
                // p->affiche_info();
                
                
                
            }else{ 
                
                if(debut_vehicule > 0 && plateau_2d[v->getLigne()][case_gauche] < 1 ){

                    while(case_gauche > 0 && plateau_2d[v->getLigne()][case_gauche] < 1 ){
                        case_gauche = case_gauche - 1;
                    }
                    // Pour eviter que toutes les situation partages les même pointeurs de véhicules
                    Vehicule* v2 = new Vehicule(v);

                    if(plateau_2d[v->getLigne()][case_gauche] > 0){
                        v2->setPosition(v->getLigne(), (case_gauche + 1));
                    }else{
                        v2->setPosition(v->getLigne(), (case_gauche));
                    }


                    //Pour avoir des pointeurs differents entre les éléments des situation, il faut copier tous les éléments
                    //Recopie du tableau de véhicule
                    std::vector<Vehicule*> nouveau_vehicules;

                    for(int j = 0; j < m_vehicules.size(); ++j){
                        nouveau_vehicules.push_back(new Vehicule(m_vehicules[j]));
                    }

                    //Remplacer le véhicule par un autre véhicule avec les nouvelles coordonnées
                    nouveau_vehicules[i] = v2;

                    Plateau* p;

                    if(v->vehiculeEgal(m_vehiculeSortant)){
                        p = new Plateau(m_sortieC, m_sortieL, v2, nouveau_vehicules);
                    }else{
                        p = new Plateau(m_sortieC, m_sortieL, m_vehiculeSortant, nouveau_vehicules);
                    }
                    m_voisins.push_back(p);

                    //p->affiche_plateau();

                }
            }
            //------------------------------------------------------------------------------------------------------------------
        }else{
            int fin_vehicule = v->getLigne() + longeur-1;  //Le moins 1 permet de s'adapter au indices partant de 0
            int debut_vehicule = v->getLigne();


            int case_bas = fin_vehicule + 1;
            int case_haut = debut_vehicule - 1;

            //Peu t'il aller en bas ?

            if(fin_vehicule < plateau_2d.size()-1 && plateau_2d[case_bas][v->getColonne()] < 1 ){

                while(case_bas < plateau_2d.size()-1 && plateau_2d[case_bas][v->getColonne()] < 1 ){
                    case_bas = case_bas + 1;
                }

                //Dans ce cas nous pouvons donc créer une nouvelle situation ou la voiture en question s'est déplacé 

                //On change la position du vehicule
                //on s'assure que les nouvelles valeurs représentes biens la partie a droite des véhicules horitontau

                // Pour eviter que toutes les situation partages les même pointeurs de véhicules
                Vehicule* v2 = new Vehicule(v);

                if(plateau_2d[case_bas][v->getColonne()] > 0){              //Cas d'arret, il y a colition avec un autree véhicuke
                    v2->setPosition((case_bas - longeur ), v->getColonne());

                }else{                                                      // Cas d'arret on atteint le bord de la grille
                    v2->setPosition((case_bas - longeur + 1), v->getColonne());

                }
                //Pour avoir des pointeurs differents entre les éléments des situation, il faut copier tous les éléments
                //Recopie du tableau de véhicule
                std::vector<Vehicule*> nouveau_vehicules;

                for(int j = 0; j < m_vehicules.size(); ++j){
                    nouveau_vehicules.push_back(new Vehicule(m_vehicules[j]));
                }

                //Remlacer le véhicule par un autre véhicule avec les nouvelles coordonnées
                nouveau_vehicules[i] = v2;

                Plateau* p;

                if(v->vehiculeEgal(m_vehiculeSortant)){
                    p = new Plateau(m_sortieC, m_sortieL, v2, nouveau_vehicules);
                }else{
                    p = new Plateau(m_sortieC, m_sortieL, m_vehiculeSortant, nouveau_vehicules);
                }
                    
                m_voisins.push_back(p);

                //p->affiche_plateau();
                //p->affiche_info();

            }else{ 
             
                if(debut_vehicule > 0 && plateau_2d[case_haut][v->getColonne()] < 1 ){

                    while(case_haut > 0 && plateau_2d[case_haut][v->getColonne()] < 1 ){
                        case_haut = case_haut - 1;
                    }

                    // Pour eviter que toutes les situation partages les même pointeurs de véhicules
                    Vehicule* v2 = new Vehicule(v);

                    if(plateau_2d[case_haut][v->getColonne()] > 0){
                        v2->setPosition((case_haut+1), v->getColonne());   //+1 car on regardais une case avant la voiture avec case_droite
                    }else{
                        v2->setPosition((case_haut), v->getColonne());   //Nous avons toucher le bord 
                    }

                    //Pour avoir des pointeurs differents entre les éléments des situation, il faut copier tous les éléments
                    //Recopie du tableau de véhicule
                    std::vector<Vehicule*> nouveau_vehicules;

                    for(int j = 0; j < m_vehicules.size(); ++j){
                        nouveau_vehicules.push_back(new Vehicule(m_vehicules[j]));
                    }

                    //Remlacer le véhicule par un autre véhicule avec les nouvelles coordonnées
                    nouveau_vehicules[i] = v2;


                    Plateau* p;

                    if(v->vehiculeEgal(m_vehiculeSortant)){
                        p = new Plateau(m_sortieC, m_sortieL, v2, nouveau_vehicules);
                    }else{
                        p = new Plateau(m_sortieC, m_sortieL, m_vehiculeSortant, nouveau_vehicules);
                    }

                    m_voisins.push_back(p);

                    //p->affiche_plateau();
                    // p->affiche_info();
                    
                }
            }
        }
    }

}



bool Plateau::plateauEgal(Plateau* otherPlateau){
    int i = 0;
    std::vector<Vehicule *> otherVehicules = otherPlateau->getVehicules();

    while(i < m_vehicules.size() - 1 && m_vehicules[i]->vehiculeEgal(otherVehicules[i])){
        i++;
    }


    return m_vehicules[i]->vehiculeEgal(otherVehicules[i]) && m_vehiculeSortant->vehiculeEgal(otherPlateau->getVehiculeSortant());
}




std::vector<Plateau*> Plateau::getVoisins()
{
    return m_voisins;
}

Plateau::~Plateau()
{
    //desalocation des véhicules du tableau
    for (unsigned int i = 0; i < m_vehicules.size(); i++) {
        delete m_vehicules[i];
    }
}