#include "Graphe.h"
#include <cmath>

const float inf= std::numeric_limits<float>::infinity();


Graphe::Graphe(Plateau* depart) : m_depart(depart), m_situationGagnante(nullptr)
{
}

Graphe::~Graphe()
{
}


//Vérifie si un plateau (noeud) est déja dans le graphe ou non 
Plateau* Graphe::contientPlateau(Plateau* otherPlateau){

    // int i = 0;
    // Plateau* dedans = nullptr;
    // while(i < m_graph.size() && dedans == false){
    //     if(m_graph[i]->plateauEgal(otherPlateau)){
    //         dedans = m_graph[i];
    //     }
    //     i++;
    // }


//Version du prof
for(unsigned i= 0; i < m_graph.size(); i++)
    if(m_graph[i]->plateauEgal(otherPlateau))
        return m_graph[i];

return nullptr;
    
}


Plateau* Graphe::getSommet(){
    return m_depart;
}


void Graphe::parcoursLargeur()
{

    bool found = false;
    Plateau* situationGagnant = nullptr;
    m_graph.push_back(m_depart);

    
    //on ajoute le sommet de départ aux sommets à traiter
    m_file.push(m_depart);

    m_depart->setVisite(true);
    //ajout du plateau de départ dans la file

    std::cout << "Traitement en cours ... " << std::endl;

    //tant qu'il reste des sommets à traiter et que nous avons pas encore trouvé la fin 
    while (!m_file.empty() && found == false) {
        //traite le premier élément de la file : au début c'est notre plateau de départ m_depart
        Plateau *p = m_file.front();
            

        //on génère les voisins de ce Plateau, situation générés avec pour chacune 1 déplacement différent
        p->deplacement_vehicule();

        //on parcours ses voisins
        std::vector<Plateau*> voisins = p->getVoisins();

        for (int i = 0; i < voisins.size(); i++) {

    
            //si ce voisin n'a pas encore été visité, alors on lui rend visite
            //S'il n'est pas contenue dans le graphe : il n'a pas encore été visité

                if(contientPlateau(voisins[i]) == nullptr){
                    m_graph.push_back(p->getVoisins()[i]);
                    m_file.push(p->getVoisins()[i]);

                }else{
                    Plateau* t = contientPlateau(voisins[i]);
                    p->getVoisins()[i]->incrNbCoups(t->getNbCoups());
                }

                //pour chacune des situations, on incrémente le nb de coup par la plus petite distance

                if(p->getVoisins()[i]->getNbCoups() > p->getNbCoups()+1){
                    p->getVoisins()[i]->incrNbCoups(p->getNbCoups()+1);
                }
                
        }

        if (p->gagnant()) {
            found = true;
            std::cout << "Situation de jeu gagnante trouvée" << std::endl;
            std::cout << "En " << p->getNbCoups() <<" coups" <<std::endl;
            std::cout << std::endl;
            p->affiche_plateau();
            situationGagnant = p;
            m_situationGagnante = p;
        }

        //supression du premier élément (à l'avant) de la file
        m_file.pop();
    }



    if(situationGagnant != nullptr){
        std::cout << "Voici la route la plus courte pour y aller " << std::endl;
        std::vector<Plateau*> cheminPlusCourt = shortestRoad(situationGagnant);

        afficheChemin(cheminPlusCourt);

        std::cout << "--------------------FIN--------------------" << std::endl;

    }else{
        std::cout << "Puzzle impossible ... " << std::endl;
    }
}

std::vector<Plateau*> Graphe::shortestRoad(Plateau* p){
    
    std::vector<Plateau*> chemin;
    chemin.push_back(p);
    Plateau* cur = p;
    Plateau* curVoisin = nullptr;
    Plateau* nexCur = nullptr;

    while(!cur->plateauEgal(m_depart)){

        int minNbCoup = inf;
        
    //Vérifie le nombres de coups de chaques voisin de la situation courrante 
    //Choisi le nomnbre de coup le plus bas et le dispose dans le tableau de chemin

        for(int i = 0; i < cur->getVoisins().size(); i++){

            curVoisin = contientPlateau(cur->getVoisins()[i]);

            if( curVoisin != nullptr && minNbCoup > curVoisin->getNbCoups()){
                nexCur = curVoisin;
                minNbCoup = curVoisin->getNbCoups();
            }
        }


        chemin.push_back(nexCur);
        cur = nexCur;
        
    }
    return chemin;

}


void Graphe::afficheChemin(std::vector<Plateau*> chemin){

    for(int i = chemin.size() - 1; i >= 0; i--){
            std::cout <<"Coup n°" << chemin.size() - i -1 << std::endl;
            chemin[i]->affiche_plateau();
            std::cout << std::endl;
    }
}