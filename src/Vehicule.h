#ifndef VEHICULE_H
#define VEHICULE_H

#include <iostream>

class Vehicule
{
    public:
        Vehicule();

        //constructeur d'un Vehicule
        //sens : 1 => horizontale
        //       0 => verticale
        Vehicule(int ligne, int colonne, int longueur, int sens);
        Vehicule(Vehicule* v);
        void print_info();
        int getLongeur() const;
        int getSens() const;
        int getLigne() const;
        int getColonne() const;
        bool vehiculeEgal(Vehicule* other);
        void setPosition(int ligne, int colonne);
    
    private:
        int m_longueur;
        int m_sens;
        int m_ligne;
        int m_colonne;


};

#endif