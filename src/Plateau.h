#ifndef PLATEAU_H
#define PLATEAU_H

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include "Vehicule.h"

//représentation de la situation avec l'ensemble des voitures sur le plateau
class Plateau
{
    public:
        Plateau();
        Plateau(const std::string &filename);
        Plateau(int sortieC, int sortieL, Vehicule* vehiculeSortant, std::vector<Vehicule*> vehicules);
        ~Plateau();

        void affiche_info();
        void affiche_plateau();
        void deplacement_vehicule();
        void incrNbCoups(int i);
        void setVisite(bool visite);
        bool estVisite() const;
        int getNbCoups() const;
        Vehicule * getVehiculeSortant();
        std::vector<Vehicule *> getVehicules();

        std::vector<Plateau *> getVoisins();
        bool gagnant();
        bool plateauEgal(Plateau* otherPlateau);


    private:
        //inqude si le Plateau a été parcouru 
        bool m_visite;
        int m_nbCoups;

        int m_sortieC;
        int m_sortieL;

        Vehicule *m_vehiculeSortant;

        std::vector<Vehicule*> m_vehicules;

        //Case à 0, case vide 
        //Case à 1, une partie de véhicule dans la case
        std::vector<std::vector<int>> plateau_2d;

        std::vector<Plateau*> m_voisins;


};

#endif