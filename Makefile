BIN = rush-hour

OBJ = ./obj/main.o \
	  ./obj/Vehicule.o \
	  ./obj/Plateau.o \
	  ./obj/Graphe.o

CC = g++

CFLAGS = -g -Wall -Wextra

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(OBJ) -o $(BIN)

./obj/main.o: ./src/main.cpp
	$(CC) $(CFLAGS) -c ./src/main.cpp -o ./obj/main.o

./obj/App.o: ./src/App.cpp ./src/App.h
	$(CC) $(CFLAGS) -c ./src/App.cpp -o ./obj/App.o

./obj/Image.o: ./src/Image.cpp ./src/Image.h
	$(CC) $(CFLAGS) -c ./src/Image.cpp -o ./obj/Image.o

./obj/Vehicule.o: ./src/Vehicule.cpp ./src/Vehicule.h
	$(CC) $(CFLAGS) -c ./src/Vehicule.cpp -o ./obj/Vehicule.o

./obj/Plateau.o: ./src/Plateau.cpp ./src/Plateau.h
	$(CC) $(CFLAGS) -c ./src/Plateau.cpp -o ./obj/Plateau.o

./obj/Graphe.o: ./src/Graphe.cpp ./src/Graphe.h
	$(CC) $(CFLAGS) -c ./src/Graphe.cpp -o ./obj/Graphe.o

clean:
	rm $(OBJ)

fclean: clean
	rm $(BIN) rush-hour.exe